<?php

namespace ProjectBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Geocode
 *
 * @ORM\Table(name="geocode")
 * @ORM\Entity(repositoryClass="ProjectBundle\Repository\GeocodeRepository")
 */
class Geocode
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="latitude", type="float")
     */
    private $latitude;

    /**
     * @var float
     *
     * @ORM\Column(name="longitude", type="float")
     */
    private $longitude;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     *
     * @return Geocode
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     *
     * @return Geocode
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    public function getDistanceTo(Geocode $other)
    {
        return self::getDistanceBetween($this, $other);
    }

    public static function getDistanceBetween(Geocode $first, Geocode $second)
    {
        $radiusOfEarth = 6371;
        $latFrom = deg2rad($first->latitude);
        $lngFrom = deg2rad($first->longitude);
        $latTo = deg2rad($second->latitude);
        $lngTo = deg2rad($second->longitude);
        $diffLatitude = $latTo - $latFrom;
        $diffLongitude = $lngTo - $lngFrom;

        $a = pow(sin($diffLatitude / 2), 2) + cos($latFrom) * cos($latTo) * pow(sin($diffLongitude / 2), 2);
        $c = 2 * asin(sqrt($a));
        $distance = $radiusOfEarth * $c;

        return $distance;
    }
}