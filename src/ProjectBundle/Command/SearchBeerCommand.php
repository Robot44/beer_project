<?php

namespace ProjectBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SearchBeerCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:search')
            ->setDescription('Searches for beer')
            ->addArgument('lat', InputArgument::REQUIRED, 'Start latitude')
            ->addArgument('lng', InputArgument::REQUIRED, 'Start longitude')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $service = $this->getContainer()->get('app.algorithm');
        $importService = $this->getContainer()->get('app.data_import');
        $importService->importData();
        if (!$service->isPositionValid(floatval($input->getArgument('lat')), floatval($input->getArgument('lng')))) {
            $output->writeln('Invalid input');
            return;
        }
        $start = microtime(true);
        $route = $service->findBeerRouteFrom($input->getArgument('lat'), $input->getArgument('lng'));
        $response = [
            'timeElapsed' => microtime(true) - $start,
            'route' => $route
        ];
        $message = $this->formWriteMessage($response);
        $output->writeln($message);
    }

    private function formWriteMessage($response)
    {
        if (!$response['route']) {
            return "There are no breweries around that are worth visiting";
        }
        $writeMessage = "\n";
        $writeMessage .= "Found ".(sizeof($response['route'])-2)." beer factories: \n";
        $prev = $response['route']->getStart()->getGeocode();
        foreach ($response['route']->getBreweries() as $ele) {
            $writeMessage .= $ele->getOldId()." ".$ele->getName()
                .": ".$ele->getGeocode()->getLatitude()
                ." ".$ele->getGeocode()->getLongitude()
                ." distance ".round($prev->getDistanceTo($ele->getGeocode()), 1)
                ."km\n";
            $prev = $ele->getGeocode();
        }
        $writeMessage .= "\nTotal distance travelled: ".round($response['route']->getDistanceTraveled(), 1)."km\n";
        $writeMessage .= "\nCollected ".sizeof($response['route']->getUniqueBeersBought())." beer types:\n";
        foreach ($response['route']->getUniqueBeersBought() as $ele) {
            $writeMessage .= $ele->getName()."\n";
        }

        $writeMessage.="\nTime elapsed: ".round($response['timeElapsed'], 2)."s\n";

        return $writeMessage;
    }
}
