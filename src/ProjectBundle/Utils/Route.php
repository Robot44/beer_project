<?php

namespace ProjectBundle\Utils;

use ProjectBundle\Entity\Brewery;
use ProjectBundle\Entity\Geocode;
use ProjectBundle\Service\Algorithm;

class Route
{
    private $breweries;

    private $distance;

    private $distnaceTraveled;

    private $uniqueBeersBought;

    /**
     * Route constructor.
     */
    public function __construct()
    {
        $this->breweries = [];
        $this->uniqueBeersBought = [];
        $this->distnaceTraveled = 0;
        $this->distance = 0;
    }


    public function setStart(float $lat, float $lng)
    {
        $startGeocode = new Geocode();
        $startGeocode->setLatitude($lat);
        $startGeocode->setLongitude($lng);
        $startBrewery = new Brewery();
        $startBrewery->setName('Home');
        $startBrewery->setOldId(-1);
        $startBrewery->setGeocode($startGeocode);
        $this->breweries = [$startBrewery];
    }

    public function setFinish()
    {
        if (!empty($this->breweries)) {
            $this->breweries[] = $this->breweries[0];
        }
    }

    public function addBrewery(Brewery $brewery)
    {
        $this->distnaceTraveled += end($this->breweries)->getGeocode()->getDistanceTo($brewery->getGeocode());
        $this->breweries[] = $brewery;
        $this->uniqueBeersBought = array_merge($this->uniqueBeersBought,
            Algorithm::beer_diff($brewery->getBeers(), $this->uniqueBeersBought)
        );
    }

    public function getDistance()
    {
        return $this->distance;
    }

    public function setDistance(float $distance)
    {
        $this->distance = $distance;
    }

    public function getDistanceTraveled()
    {
        return $this->distnaceTraveled;
    }

    public function getDistanceLeft()
    {
        return $this->distance - $this->distnaceTraveled;
    }

    public function getUniqueBeersBought(): array
    {
        return $this->uniqueBeersBought;
    }

    public function getStart() : Brewery
    {
        return $this->breweries[0];
    }

    public function getLast() : Brewery
    {
        return end($this->breweries);
    }

    public function getBreweries()
    {
        return $this->breweries;
    }
}