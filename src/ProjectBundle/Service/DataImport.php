<?php

namespace ProjectBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use ProjectBundle\Entity\Beer;
use ProjectBundle\Entity\Brewery;
use ProjectBundle\Entity\Geocode;

class DataImport
{
    const GEOCODES_PATH = "https://raw.githubusercontent.com/brewdega/open-beer-database-dumps/master/dumps/geocodes.csv";
    const BREWERIES_PATH = "https://raw.githubusercontent.com/" . "brewdega/open-beer-database-dumps/master/dumps/breweries.csv";
    const BEERS_PATH = "https://raw.githubusercontent.com/brewdega/open-beer-database-dumps/master/dumps/beers.csv";

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * Helper constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    public function importData()
    {
        if ($this->isDataLoaded()) {
            return;
        }
        echo "Import started\n";
        $this->deleteDataLoaded();
        echo "Importing breweries\n";
        $this->loadBreweries();
        echo "Importing beers\n";
        $this->loadBeers();
        echo "Importing locations\n";
        $this->loadLocations();
    }

    private function isDataLoaded()
    {
        $beers = $this->em->getRepository('ProjectBundle:Beer')->getCount();
        $breweries = $this->em->getRepository('ProjectBundle:Brewery')->getCount();
        $geocodes = $this->em->getRepository('ProjectBundle:Geocode')->getCount();

        if ($beers == 0 || $breweries == 0 || $geocodes == 0) {

            return false;
        }

        return true;
    }

    private function deleteDataLoaded()
    {
        $this->em->getRepository('ProjectBundle:Beer')->deleteAll();
        $this->em->getRepository('ProjectBundle:Brewery')->deleteAll();
        $this->em->getRepository('ProjectBundle:Geocode')->deleteAll();
    }

    private function loadBreweries()
    {
        $rowCount = 0;
        $batchSize = 20;
        if (($handle = fopen(self::BREWERIES_PATH, "r")) !== false) {
            while (($line = fgetcsv($handle)) !== false) {
                if ($rowCount === 0) {
                    $rowCount++;
                    continue;
                }
                $rowCount++;
                $brewery = new Brewery();
                $brewery->setName($line[1]);
                $brewery->setOldId($line[0]);
                $this->em->persist($brewery);
                if (($rowCount % $batchSize) === 0) {
                    $this->em->flush();
                    $this->em->clear();
                }
            }
            $this->em->flush();
            $this->em->clear();
            fclose($handle);
        }
    }

    private function loadBeers()
    {
        $rowCount = 0;
        $batchSize = 20;

        if (($handle = fopen(self::BEERS_PATH, "r")) !== false) {
            while (($line = fgetcsv($handle)) !== false) {
                if ($rowCount === 0) {
                    $rowCount++;
                    continue;
                }
                $beer = new Beer();
                $beer->setName($line[2]);
                $beer->setType($line[4]);
                $brewery = $this->em->getRepository('ProjectBundle:Brewery')->findBreweryBy(['oldId' => $line[1]]);
                if ($brewery) {
                    $brewery->addBeer($beer);;
                }
                $this->em->persist($beer);
                if (($rowCount % $batchSize) === 0) {
                    $this->em->flush();
                    $this->em->clear();
                };
            }
            $this->em->flush();
            $this->em->clear();
            fclose($handle);
        }
    }

    private function loadLocations()
    {
        $rowCount = 0;
        $batchSize = 20;
        if (($handle = fopen( self::GEOCODES_PATH, "r")) !== false) {
            while (($line = fgetcsv($handle)) !== false) {
                if ($rowCount === 0) {
                    $rowCount++;
                    continue;
                }
                $geocode = new Geocode();
                $geocode->setLatitude($line[2]);
                $geocode->setLongitude($line[3]);
                $brewery = $this->em->getRepository('ProjectBundle:Brewery')->findBreweryBy(['oldId' => $line[1]]);
                if ($brewery) {
                    $brewery->setGeocode($geocode);
                }
                $this->em->persist($geocode);
                if (($rowCount % $batchSize) === 0) {
                    $this->em->flush();
                    $this->em->clear();
                }
            }
            $this->em->flush();
            $this->em->clear();
            fclose($handle);
        }
    }
}