<?php

namespace ProjectBundle\Repository;

use ProjectBundle\Entity\Geocode;

class BreweryRepository extends \Doctrine\ORM\EntityRepository
{
    public function getCount()
    {
        return $this->getEntityManager()
            ->getRepository('ProjectBundle:Brewery')
            ->createQueryBuilder('br')
            ->select('count(br.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function deleteAll()
    {
        return $this->getEntityManager()
            ->getRepository('ProjectBundle:Brewery')
            ->createQueryBuilder('br')
            ->delete()
            ->getQuery()
            ->execute();
    }

    public function findFirst()
    {
        return $this->getEntityManager()
            ->getRepository('ProjectBundle:Brewery')
            ->createQueryBuilder('br')
            ->orderBy('br.id', 'ASC')
            ->setMaxResults(1)
            ->getQuery()
            ->getSingleResult();
    }

    public function findBreweryBy(array $criteria)
    {
        $qb = $this->getEntityManager()
            ->getRepository('ProjectBundle:Brewery')
            ->createQueryBuilder('br')
            ->setMaxResults(1);
        if (!empty($criteria['oldId'])) {
            $qb->andWhere('br.oldId = :oldId')
                ->setParameter('oldId', $criteria['oldId']);
        }
        return $qb->getQuery()->getOneOrNullResult();
    }

    public function findBreweriesInRange($range, Geocode $position, $visited = null)
    {
        $qb = $this->getEntityManager()
            ->getRepository('ProjectBundle:brewery')
            ->createQueryBuilder('br')
            ->select('br, g, b')
            ->innerJoin('br.geocode', 'g')
            ->innerJoin('br.beers', 'b')
            ->where('(6371 * acos(cos(radians(g.latitude)) * cos(radians(:lat )) * cos(radians(:lng) - radians(g.longitude)) + sin(radians(g.latitude)) * sin(radians(:lat)))) < :range')
            ->setParameters([
                'lat' => $position->getLatitude(),
                'lng' => $position->getLongitude(),
                'range' => $range,
            ]);

        is_null($visited) ?: array_shift($visited);
        if (!empty($visited)) {
            $qb->andWhere('br NOT IN (:visited)')
                ->setParameter('visited', $visited);
        }
        return $qb->getQuery()
            ->getResult();
    }
}
