### How do I get set up? ###
 
## Setup:
1. composer install
2. php bin/console doctrine:migrations:migrate
3. Done
 
* Command: `php bin/console app:search [lat] [lng]`
* Usage example: `php bin/console app:search 51.1156156 11.15615615`