<?php

namespace ProjectBundle\Repository;

class BeerRepository extends \Doctrine\ORM\EntityRepository
{
    public function getCount()
    {
        return $this->getEntityManager()
            ->getRepository('ProjectBundle:Beer')
            ->createQueryBuilder('b')
            ->select('count(b.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function deleteAll()
    {
        return $this->getEntityManager()
            ->getRepository('ProjectBundle:Beer')
            ->createQueryBuilder('b')
            ->delete()
            ->getQuery()
            ->execute();
    }

    public function findBeersBy(array $criteria)
    {
        return $this->getEntityManager()
            ->getRepository('ProjectBundle:Beer')
            ->createQueryBuilder('b')
            ->where(':brewery MEMBER OF b.breweries')
            ->andWhere('b.type NOT IN (:types)')
            ->setParameters([
                'brewery' => $criteria['brewery'],
                'types' => $criteria['types']
            ])
            ->getQuery()
            ->getResult();
    }
}
