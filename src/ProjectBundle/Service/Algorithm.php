<?php

namespace ProjectBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use ProjectBundle\Entity\Brewery;
use ProjectBundle\Utils\Route;

class Algorithm
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    public function isPositionValid($lat, $lng)
    {
        if ((-90.0 > $lat)||($lat > 90.0)) {
            return false;
        }
        if ((-180.0 > $lng)||($lng > 180.0)) {
            return false;
        }
        return true;
    }

    static function beer_diff($a, $b)
    {
        $map = $out = array();
        foreach($b as $val) $map[$val->getType()] = 1;
        foreach($a as $val) if(!isset($map[$val->getType()])) {
            $out[] = $val;
            $map[$val->getType()] = 1;
        }

        return $out;
    }

    private function getSortedByValueBreweries($breweries, Route $route)
    {
        $result = [];
        foreach ($breweries as $ele) {
            $currentPosition = $route->getLast()->getGeocode();
            $startPosition = $route->getStart()->getGeocode();
            $distanceTo = $currentPosition->getDistanceTo($ele->getGeocode());
            $distanceFrom = $ele->getGeocode()->getDistanceTo($startPosition);
            $res = $route->getDistanceLeft() - $distanceTo - $distanceFrom;
            if ($res > 0) {
                $beers =  self::beer_diff($ele->getBeers(), $route->getUniqueBeersBought());
                if (!empty($beers)) {
                    $result[] = ['brewery' => $ele, 'beers' => $beers, 'distanceTo' => $distanceFrom];
                }
            }
        }

        usort($result, function ($a, $b) {
            $ca = $a['distanceTo'] / sizeof($a['beers']);
            $cb = $b['distanceTo'] / sizeof($b['beers']);
            return $ca > $cb;
        });

        return array_column($result, 'brewery');
    }

    public function findBeerRouteFrom(float $lat, float $lng)
    {
        $route = new Route();
        $route->setStart($lat, $lng);
        $route->setDistance(2000);

        do {
            $breweriesInRange = $this->em->getRepository('ProjectBundle:Brewery')
                ->findBreweriesInRange(
                    $route->getDistanceLeft()/2,
                    $route->getStart()->getGeocode(),
                    $route->getBreweries()
                );
            $breweriesWorthVisiting = $this->getSortedByValueBreweries(
                $breweriesInRange,
                $route
            );
            if (empty($breweriesWorthVisiting)) {
                break;
            }
            $route->addBrewery($breweriesWorthVisiting[0]);
            array_shift($breweriesWorthVisiting);
        } while (!empty($breweriesWorthVisiting));

        if ($route->getDistanceTraveled() == 0) {
            return null;
        }
        $route->setFinish();
        return $route;
    }
}
