<?php

namespace Test\ProjectBundle\Service;

use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;
use ProjectBundle\Entity\Beer;
use ProjectBundle\Service\Algorithm;

class AlgorithmTest extends TestCase
{
    /**
     * @dataProvider positionProvider
     */
    public function testIsPositionValid($lat, $lng, $expected)
    {
        $em = $this->createMock(EntityManager::class);
        $newAlgorithm = new Algorithm($em);

        self::assertEquals($newAlgorithm->isPositionValid($lat, $lng), $expected);
    }

    public function testShouldntHaveAnyDifferentBeer()
    {
        $list1 = $this->generateBeerList(10,1);
        $list2 = $this->generateBeerList(5,2);
        $diff = Algorithm::beer_diff($list2, $list1);

        self::assertEquals(0, sizeof($diff));
    }

    public function positionProvider()
    {
        return [
            [-1, 15, true],
            [-89, 179, true],
            [1, 1000, false],
            [10000, -10000, false]
        ];
    }

    public function generateBeerList($n,$i)
    {
        $list = [];
        for ($j = $i; $j < $i+$n; $j++) {
            $newBeer = new Beer();
            $newBeer->setType($j);
            $list[] = $newBeer;
        }
        return $list;
    }
}