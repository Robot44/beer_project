<?php

namespace ProjectBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Beer
 *
 * @ORM\Table(name="beer")
 * @ORM\Entity(repositoryClass="ProjectBundle\Repository\BeerRepository")
 */
class Beer
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="integer")
     */
    private $type;

    /**
     * @ORM\ManyToMany(targetEntity="Brewery", mappedBy="beers")
     */
    private $breweries;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Beer
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return Beer
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->breweries = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add brewery
     *
     * @param \ProjectBundle\Entity\Brewery $brewery
     *
     * @return Beer
     */
    public function addBrewery(\ProjectBundle\Entity\Brewery $brewery)
    {
        $this->breweries[] = $brewery;

        return $this;
    }

    /**
     * Remove brewery
     *
     * @param \ProjectBundle\Entity\Brewery $brewery
     */
    public function removeBrewery(\ProjectBundle\Entity\Brewery $brewery)
    {
        $this->breweries->removeElement($brewery);
    }

    /**
     * Get breweries
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBreweries()
    {
        return $this->breweries;
    }
}
