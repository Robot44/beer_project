<?php

namespace ProjectBundle\Repository;

class GeocodeRepository extends \Doctrine\ORM\EntityRepository
{
    public function getCount()
    {
        return $this->getEntityManager()
            ->getRepository('ProjectBundle:Geocode')
            ->createQueryBuilder('g')
            ->select('count(g.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function deleteAll()
    {
        return $this->getEntityManager()
            ->getRepository('ProjectBundle:Geocode')
            ->createQueryBuilder('g')
            ->delete()
            ->getQuery()
            ->execute();
    }
}
