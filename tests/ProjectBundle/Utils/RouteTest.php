<?php

namespace Test\ProjectBundle\Utils;

use PHPUnit\Framework\TestCase;
use ProjectBundle\Entity\Beer;
use ProjectBundle\Entity\Brewery;
use ProjectBundle\Entity\Geocode;
use ProjectBundle\Utils\Route;

class RouteTest extends TestCase
{
    public function testDistanceZeroWhenNoOtherBreweries()
    {
        $route = new Route();
        $route->setStart(44,45);
        $route->setFinish();

        self::assertEquals(0, $route->getDistance());
        self::assertEquals(0, $route->getDistanceLeft());
        self::assertEquals(0, $route->getDistance());
    }

    public function testAddsBreweryCorrectly()
    {
        $route = new Route();
        $route->setStart(44,45);
        $geocode = new Geocode();
        $geocode->setLatitude(10);
        $geocode->setLongitude(10);
        $brewery = new Brewery();
        $brewery->setGeocode($geocode);
        for ($i = 0; $i < 3; $i++) {
            $beer = new Beer();
            $beer->setType($i);
            $brewery->addBeer($beer);
        }
        $route->addBrewery($brewery);

        self::assertEquals(2, sizeof($route->getBreweries()));
        self::assertEquals(3, sizeof($route->getUniqueBeersBought()));
        self::assertEquals(
            Geocode::getDistanceBetween($route->getStart()->getGeocode(), $geocode),
            $route->getDistanceTraveled(),
            '',
            0.1
        );

        return $route;
    }

    /**
     * @depends testAddsBreweryCorrectly
     */
    public function testSetsFinishCorrectly(Route $route)
    {
        $distanceTraveled = $route->getDistanceTraveled();
        $beers = $route->getUniqueBeersBought();
        $route->setFinish();
        self::assertEquals(
            $distanceTraveled + Geocode::getDistanceBetween($route->getStart()->getGeocode(),
                $route->getLast()->getGeocode()),
            $route->getDistanceTraveled(),
            '',
            0.1
        );
        self::assertEquals(sizeof($beers), sizeof($route->getUniqueBeersBought()));
    }
}