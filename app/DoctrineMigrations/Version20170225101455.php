<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170225101455 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE beer (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, type INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE geocode (id INT AUTO_INCREMENT NOT NULL, latitude DOUBLE PRECISION NOT NULL, longitude DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE brewery (id INT AUTO_INCREMENT NOT NULL, geocode_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_1A599547753820EA (geocode_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE breweries_beers (brewery_id INT NOT NULL, beer_id INT NOT NULL, INDEX IDX_9BDB1605D15C960 (brewery_id), INDEX IDX_9BDB1605D0989053 (beer_id), PRIMARY KEY(brewery_id, beer_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE brewery ADD CONSTRAINT FK_1A599547753820EA FOREIGN KEY (geocode_id) REFERENCES geocode (id)');
        $this->addSql('ALTER TABLE breweries_beers ADD CONSTRAINT FK_9BDB1605D15C960 FOREIGN KEY (brewery_id) REFERENCES brewery (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE breweries_beers ADD CONSTRAINT FK_9BDB1605D0989053 FOREIGN KEY (beer_id) REFERENCES beer (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE breweries_beers DROP FOREIGN KEY FK_9BDB1605D0989053');
        $this->addSql('ALTER TABLE brewery DROP FOREIGN KEY FK_1A599547753820EA');
        $this->addSql('ALTER TABLE breweries_beers DROP FOREIGN KEY FK_9BDB1605D15C960');
        $this->addSql('DROP TABLE beer');
        $this->addSql('DROP TABLE geocode');
        $this->addSql('DROP TABLE brewery');
        $this->addSql('DROP TABLE breweries_beers');
    }
}
