<?php

namespace ProjectBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Brewery
 *
 * @ORM\Table(name="brewery")
 * @ORM\Entity(repositoryClass="ProjectBundle\Repository\BreweryRepository")
 */
class Brewery
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="oldId", type="integer")
     */
    private $oldId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="Geocode")
     * @ORM\JoinColumn(name="geocode_id", referencedColumnName="id")
     */
    private $geocode;

    /**
     * @ORM\ManyToMany(targetEntity="Beer", inversedBy="breweries")
     * @ORM\JoinTable(name="breweries_beers")
     */
    private $beers;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Brewery
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->beers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set geocode
     *
     * @param \ProjectBundle\Entity\Geocode $geocode
     *
     * @return Brewery
     */
    public function setGeocode(\ProjectBundle\Entity\Geocode $geocode = null)
    {
        $this->geocode = $geocode;

        return $this;
    }

    /**
     * Get geocode
     *
     * @return \ProjectBundle\Entity\Geocode
     */
    public function getGeocode()
    {
        return $this->geocode;
    }

    /**
     * Add beer
     *
     * @param \ProjectBundle\Entity\Beer $beer
     *
     * @return Brewery
     */
    public function addBeer(\ProjectBundle\Entity\Beer $beer)
    {
        $this->beers[] = $beer;

        return $this;
    }

    /**
     * Remove beer
     *
     * @param \ProjectBundle\Entity\Beer $beer
     */
    public function removeBeer(\ProjectBundle\Entity\Beer $beer)
    {
        $this->beers->removeElement($beer);
    }

    /**
     * Get beers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBeers()
    {
        return $this->beers;
    }

    /**
     * Set oldId
     *
     * @param integer $oldId
     *
     * @return Brewery
     */
    public function setOldId($oldId)
    {
        $this->oldId = $oldId;

        return $this;
    }

    /**
     * Get oldId
     *
     * @return integer
     */
    public function getOldId()
    {
        return $this->oldId;
    }
}
